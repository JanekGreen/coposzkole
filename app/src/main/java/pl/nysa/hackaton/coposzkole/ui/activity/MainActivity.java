package pl.nysa.hackaton.coposzkole.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.nysa.hackaton.coposzkole.R;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnMap)
    Button map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.btnMap)
    public void onBtnClick(View view){
        Intent intent = new Intent(this,MapsActivity.class);
        startActivity(intent);
    }
}
